public with sharing class GetCoffeePrice {
    @AuraEnabled(cacheable=true)
    public static List<PricebookEntry> GetPriceById(String id){
            List<PricebookEntry> price= [
                SELECT UnitPrice
                FROM PricebookEntry p
                WHERE p.Product2Id=:id 
                //AND p.Pricebook2Id='01s4x00000025rOAAQ'
            ];
        System.debug('Debug message:'+JSON.serialize(price));
        return price;
    }
    public GetCoffeePrice() {
    }
}