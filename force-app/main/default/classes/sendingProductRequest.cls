public with sharing class sendingProductRequest {
    public sendingProductRequest() {
 }
    @AuraEnabled(cacheable=true)
    public static void getingCoffeeInfo(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://routineautomation3-dev-ed.lightning.force.com/CoffeeProducts');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        //System.debug(JSON.deserializeUntyped(response.getBody()));
          if (response.getStatusCode() == 200) {
              System.debug('Responce exist');
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            List<Object> coffees = (List<Object>) results.get('CoffeeProducts');
            System.debug('Received the following coffe:');
            for (Object coffee: coffees) {
                  System.debug(coffee);
                }
        }
    }
}