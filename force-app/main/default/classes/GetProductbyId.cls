public with sharing class GetProductbyId {
    @AuraEnabled(cacheable=true)
    public static List<ProductPriceBookEntryListWrapper> GetProductInfoId(String id){
        List<ProductPriceBookEntryListWrapper> prWrapperList = new List<ProductPriceBookEntryListWrapper>();
            List<Product2> ProductsAndPricesList= [
                SELECT Name, DuplicateId__c,Image__c
                FROM Product2 p
                WHERE p.DuplicateId__c=:id
            ];
            PricebookEntry justPrice;
            try{
            justPrice=[SELECT UnitPrice  FROM PricebookEntry  WHERE PricebookEntry.Product2Id=:id];
            }
            catch(Exception e){

            }
            if(!ProductsAndPricesList.isEmpty()){
                for(Product2 productItem : ProductsAndPricesList){
                    ProductPriceBookEntryListWrapper ProductAndPriceWrapper = new ProductPriceBookEntryListWrapper();
                    ProductAndPriceWrapper.productRecord = productItem ;
                    if(!(justPrice==NULL)){
                    ProductAndPriceWrapper.Price = justPrice ;
                    }
                    prWrapperList.add(ProductAndPriceWrapper);
                }
            }
            return prWrapperList;
    }
    public class ProductPriceBookEntryListWrapper{
        @AuraEnabled
        public Product2 productRecord{get;set;}
        @AuraEnabled
        public PricebookEntry Price{get;set;}
    }
    public GetProductbyId() {
    }
}