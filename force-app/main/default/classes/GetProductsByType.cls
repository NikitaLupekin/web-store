public with sharing class GetProductsByType {
    @AuraEnabled(cacheable=true)
    public static List<Product2> getProductbyfamily(String fam){
            List<Product2> products= [
                SELECT Name,Family, DuplicateId__c,Image__c
                FROM Product2 p
                WHERE p.Family=:fam
            ];
        System.debug('Debug message:'+JSON.serialize(products));
        return products;
    }
    public GetProductsByType() {
    }
}