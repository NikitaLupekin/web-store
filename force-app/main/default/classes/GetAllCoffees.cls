public with sharing class GetAllCoffees {
    @AuraEnabled(cacheable=true)
    public static List<Product2> GetAll(){
            List<Product2> products= [
                SELECT Name,Family, DuplicateId__c,Image__c
                FROM Product2 p
                WHERE p.RecordTypeId='0124x0000004p2HAAQ'
            ];
        System.debug('Debug message:'+JSON.serialize(products));
        return products;
    }
    public GetAllCoffees() {
    }
}