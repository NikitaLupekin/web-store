import { LightningElement,api,wire} from 'lwc';
import getProductbyfamily from '@salesforce/apex/GetProductsByType.getProductbyfamily';
import GetAll from '@salesforce/apex/GetAllCoffees.GetAll';

export default class DisplayCoffee extends LightningElement {
    @api type;
    coffees;
    /*@wire(getProductbyfamily, { fam: '$type' })
    coffees;*/
    handleTileClick(evt) {
        const event = new CustomEvent('productselected', {
            detail: evt.detail
        });
        this.dispatchEvent(event);
    }
    @api getall(){
        GetAll({

        })
        .then(coffeesresult => {
            console.log(coffeesresult);
            this.coffees=coffeesresult;
            console.log(JSON.stringify(this.coffees));
        })
        .catch(error => {
            
        });
    }
    /*handledetail(event){
        var con= this.template.querySelector('c-deatailItem');
        //con.justsetid(event.detail);  
    }*/
    @api setType(PublicType){
        this.type=PublicType;
        getProductbyfamily({ //imperative Apex call
            fam: this.type
        })
            .then(coffeesresult => {
                console.log(coffeesresult);
                this.coffees=coffeesresult;
                console.log(JSON.stringify(this.coffees));
            })
            .catch(error => {
                
            });
    }
}