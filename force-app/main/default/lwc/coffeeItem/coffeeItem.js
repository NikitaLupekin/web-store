import { LightningElement,api } from 'lwc';

export default class CoffeeItem extends LightningElement {
    @api product;
    @api key;
    itemClick() {
        const event = new CustomEvent('itemclick', {
            detail: this.product.DuplicateId__c
        });
        var temp=event.detail;
        this.dispatchEvent(event);
    }
}