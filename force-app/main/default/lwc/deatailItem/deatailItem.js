import GetPriceById from '@salesforce/apex/GetCoffeePrice.GetPriceById';
import GetProductbyId from '@salesforce/apex/GetProductbyId.GetProductInfoId';
import { api, LightningElement } from 'lwc';

export default class DeatailItem extends LightningElement {
    @api productid;
    detailinfo;
    deatailprices;
    isPrise;
    @api justsetid(ProductId){
        console.log("Getting text: "+ProductId);
        this.productid=ProductId;
        GetProductbyId({ //imperative Apex call
            id: this.productid
        })
            .then(coffeedetail => {
                this.detailinfo=coffeedetail;
                if(coffeedetail.Price){
                    this.isPrise=false;
                }
                else{
                    this.isPrise=true;
                }
                console.log("You are in succes,size:");
                console.log(JSON.stringify(this.detailinfo));
            })
            .catch(error => {
                console.log("You have an error");
                console.log(error);
            });
    }
}