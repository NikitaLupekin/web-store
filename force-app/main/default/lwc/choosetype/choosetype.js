import { LightningElement, track, wire} from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import PRODUCT_OBJECT from '@salesforce/schema/Product2';
import Type_FIELD from '@salesforce/schema/Product2.Family';
import getProductQuantity from '@salesforce/apex/sendingProductRequest.getingCoffeeInfo';

export default class Choosetype extends LightningElement {
    @track value;
    selectesItem;
    @wire(getObjectInfo, { objectApiName: PRODUCT_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '0124x0000004p2HAAQ', fieldApiName: Type_FIELD})
    TypePicklistValues;

    handleClick(event){
        console.log(this.value)
        var display= this.template.querySelector('c-display-coffee');
        if(this.value=="None"){
            display.getall();
        }
        else{
            display.setType(this.value);
        }
    }
    handleselectedproduct(event){
        getProductQuantity({

        })
        .then(coffeequantity => {
            console.log(JSON.stringify(coffeequantity));
        })
        .catch(error => {
            
        });
        [].map.call(this.template.querySelectorAll('.container'), n => { n.style.display = 'block'; })
        var credidCardForm= this.template.querySelector('c-creedit-card-form');
        credidCardForm.setFormVisable(false);
        this.selectesItem=event.detail;
        var con= this.template.querySelector('c-deatail-item');
        con.style.display="block";
        con.justsetid(event.detail);
    }
    handleChange(event) {
        this.value = event.detail.value;
        var con= this.template.querySelector('c-deatail-item');
        con.style.display ="none";
    }
    renderedCallback(){
        var con= this.template.querySelector('c-display-coffee');
        if(this.value){
        }
        else{
            con.getall();
        }
    }
    handleBuyClick(event){
        console.log("I am on handler");
        [].map.call(this.template.querySelectorAll('.container'), n => { n.style.display = 'none'; })
        let button = this.template.querySelectorAll('.lightning-button,.container');
        var credidCardForm= this.template.querySelector('c-creedit-card-form');
        credidCardForm.setFormVisable(true);
    }
}